import React, { useState, useEffect } from 'react';
import './App.css';

import marked from 'marked'
import { sampleText } from '../../sampleText'

function App() {
   
    let initialText;
    
    if(localStorage.getItem('text').length !== 0){
      console.log("pouet1")
       initialText = {
         sampleText : localStorage.getItem('text')
       } 
    } else{
      console.log("pouet2")
      initialText = {sampleText}
    
    }
    const [monState, setTexte] = useState(initialText);
    
 useEffect(() => {
   const textNouveau = monState.sampleText;
   localStorage.setItem('text', textNouveau);
 })


  let handleChange = function(event){
    const text = { ...monState}
    const newText = event.target.value;
    text.sampleText = newText;
    setTexte(text);
  }

  const renderText = texte => {
    const __html = marked(texte, {sanitize: true})
    return { __html: __html}
  }
  

  return (
   <div className="container">
     <div className="row">

      <div className="col-sm-6">
        <textarea
          onChange={handleChange}
          value = {monState.sampleText}
          className ="form-control" 
          rows="35">
          </textarea>
      </div>

      <div className="col-sm-6">
        <div 
         dangerouslySetInnerHTML={renderText(monState.sampleText)}>

        </div>
      </div>
      
     </div>
   </div>
  );
}

export default App;
